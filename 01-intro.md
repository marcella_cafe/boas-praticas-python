# Introdução  {#index}

Esse documento foi baseado no guia PEP 8 que são convenções de codificação para Python. Trata-se de recomendações, porém muitos trabalhos tem suas próprias diretrizes de estilo. Em casos de conflitos, os guias específicos tem precedência ao guia PEP 8.

Iniciaremos o documento com o Zen do Python por Tim Peters:

- Bonito é melhor que feio.
- Explicito é melhor que implícito.
- Simples é melhor que complexo.
- Complexo é melhor que complicado.
- Linear é melhor que aninhado.
- Esparso é melhor que denso.
- Legibilidade conta.
- Casos especiais não são especiais o bastante para quebrar regras. Ainda que praticidade vença pureza.
- Erros nunca devem passar silenciosamente. A menos que sejam explicitamente silenciados.
- Diante da ambiguidade, recuse a tentação de adivinhar.
- Deveria haver um – e preferencialmente só um – modo óbvio para fazer algo. Embora esse modo possa não ser óbvio a princípio.
- Agora é melhor que nunca. Embora frequentemente seja melhor que “já”.
- Se a implementação é difícil de explicar, é uma má ideia.
- Se a implementação é fácil de explicar, pode ser uma boa ideia.
- Namespaces são uma grande ideia – vamos ter mais dessas!
