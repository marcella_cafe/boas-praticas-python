# Disposição do Código {#programing}

## Identação

Use 4 espaços por nível de recuo. As linhas de continuação devem alinhar os elementos encapsulados verticalmente usando a junção de linha implícita do Python dentro de parênteses, colchetes e chaves ou usando um `recuo suspenso`.

```python
    # Errado
    # Proibidos argumentos na primeira linha ao não usar alinhamento vertical
    foo = long_function_name(var_one, var_two,
    var_three, var_four)

    # Recuo adicional necessário, pois o recuo não é distinguível
    def long_function_name(
        var_one, var_two, var_three,
        var_four):
        print(var_one)

    # Correto
    # Alinhado com o delimitador de abertura
    foo = long_function_name(var_one, var_two,
                         var_three, var_four)

    # Adicione quatro espaços para distinguir argumentos do resto
    def long_function_name(
        var_one, var_two, var_three,
        var_four):
    print(var_one)

    # Recuos suspensos devem adicionar um nível
    foo = long_function_name(
    var_one, var_two,
    var_three, var_four)
```

    

## Separadores ou Espaços

Os espaços são o método de recuo preferido. Os separadores são usados apenas para permanecer consistentes com o código que é já recuado com separadores. Python não permite a mistura de separadores e espaços para recuo.

## Comprimento da Linha

Limite todas as linhas a um máximo de 79 caracteres. Para blocos longos de texto fluidos com menos restrições estruturais (docstrings ou comentários), o comprimento da linha deve ser limitado a 72 caracteres. 
Limitar a largura necessária da janela do editor torna possível ter vários arquivos abertos lado a lado e funciona bem ao usar ferramentas de revisão do código que apresentam as duas versões em colunas adjacentes.
Barras invertidas ainda podem ser apropriadas. Por exemplo, várias instruções não puderam usar a continuação implícita do Python 3.10, então as barras invertidas eram aceitáveis para esse caso usa-se o with. 

```python
with open('/path/to/some/file/you/want/to/read') as file_1, \
     open('/path/to/some/file/being/written', 'w') as file_2:
    file_2.write(file_1.read())

```
 
## Operadores 

Durante décadas, o estilo recomendado era quebrar depois dos operadores binários. Mas isso pode prejudicar a legibilidade de duas maneiras: os operadores tendem a ficar espalhados por diferentes colunas na tela, e cada operador afasta-se de seu operando e entra na linha anterior. Nesse caso, o olho tem que fazer um trabalho extra para dizer quais itens são adicionados e quais são subtraídos:

```python
# Errado:
# Operadores ficam longe de seus operandos
income = (gross_wages +
          taxable_interest +
          (dividends - qualified_dividends) -
          ira_deduction -
          student_loan_interest)

```

Seguir a tradição da matemática geralmente resulta em mais código legível:

```python
# Correto:
# Fácil combinar operadores com operandos
income = (gross_wages
          + taxable_interest
          + (dividends - qualified_dividends)
          - ira_deduction
          - student_loan_interest)
```

## Linhas em Branco

Linhas em branco extras podem ser usadas (com moderação) para separar grupos de funções relacionadas.
Use linhas em branco nas funções, com moderação, para indicar seções lógicas.

## Importações

```python
# Errado:
import sys, os

# Correto:
import os
import sys
```

As importações são sempre colocadas na parte superior do arquivo, logo após qualquer módulo e docstrings, e antes de módulos globais e constantes.
As importações devem ser agrupadas pela seguinte ordem:

- Importações de bibliotecas padrão.
- Importações de terceiros relacionadas.
- Importações específicas de aplicativos/bibliotecas locais.

Você deve colocar uma linha em branco entre cada grupo de importações.

Recomenda-se importações absolutas, pois geralmente são mais legíveis.

```python
import mypkg.sibling
from mypkg import sibling
from mypkg.sibling import example

```
