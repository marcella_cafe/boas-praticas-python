# Functions

## Nomeação

Além de seguir o conselho geral de [nomes de objetos], tente usar verbos para nomes de funções:

```{r eval = FALSE}
# Good
add_row()
permute()

# Bad
row_adder()
permutation()
```

## Linhas longas

Se uma definição de função for executada em várias linhas, indente a segunda linha para onde a definição começa.
Caso a função tenha mais de três parâmetros coloque um em cada linha para facilitar a visualização.

```{r, eval = FALSE}
# Good
long_function_name <- function(a = "a long argument",
                               b = "another argument",
                               c = "another long argument") {
  # As usual code is indented by two spaces.
}

# Bad
long_function_name <- function(a = "a long argument",
  b = "another argument",
  c = "another long argument") {
  # Here it's hard to spot where the definition ends and the
  # code begins
}
```

## `return()`

Sempre utilize o `return()` nas funções.

```{r eval = FALSE}
# Good
find_abs <- function(x) {
  if (x > 0) {
    x <- x + 1
  }
  
  value <- x * -1
  return(value)
}

# Bad
add_two <- function(x, y) {
  return(x + y)
}

find_abs <- function(x) {
  if (x > 0) {
    x <- x + 1
  }
  x * -1
}

```

As declarações de retorno sempre devem estar em sua própria linha, pois têm efeitos importantes no fluxo de controle. Veja também [instruções em linha](#inline-statements).

```{r, eval = FALSE}
# Good
find_abs <- function(x) {
  if (x > 0) {
    x <- x + 1
  }
  
  value <- x * -1
  return(value)
}

# Bad
find_abs <- function(x) {
  if (x > 0) return(x)
  x * -1
}
